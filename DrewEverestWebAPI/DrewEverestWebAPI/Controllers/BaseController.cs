﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DrewEverestWebAPI.Controllers
{
    public class BaseController : ApiController
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            // if (Convert.ToBoolean(ConfigurationManager.AppSettings["ValidateAuthHeader"]))
            // {
            var headerOk = false;
            var authHeader = GetAuthHeader();

            if (!Equals(authHeader, null))
            {
                //_domainName = authHeader[0];
                headerOk = CheckAuthHeader(authHeader[0], authHeader[1]);
            }

            if (!headerOk)
            {
                HttpContext.Current.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                HttpContext.Current.Response.StatusDescription = "Domain name and key are either incorrect or missing from the request Authorization header.";
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.Response.SuppressFormsAuthenticationRedirect = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                return;
            }
            //  }
            //  _queryStringParser = new QueryStringParser(controllerContext.Request.RequestUri.Query);
            base.Initialize(controllerContext);
        }
        private string[] GetAuthHeader()
        {
            var headers = HttpContext.Current.Request.Headers;
            var authValue = headers.AllKeys.Contains("Authorization") ? headers["Authorization"] : String.Empty;

            // If auth value doesn't exist, get out
            if (String.IsNullOrEmpty(authValue)) return null;

            // Strip off the "Basic "
            authValue = authValue.Remove(0, 6);

            // Decode it; if empty then get out
            var authValueDecoded = DecodeBase64(authValue);
            if (String.IsNullOrEmpty(authValueDecoded)) return null;

            // Now split it to get the domain info (index 0 = domain name, index 1 = domain key)
            return authValueDecoded.Split('|');
        }
        private string DecodeBase64(string encodedValue) => System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(encodedValue));

        private bool CheckAuthHeader(string domainName, string domainKey)
        {
            // If either domain name or domain key are empty, get out
            if (String.IsNullOrEmpty(domainName) || String.IsNullOrEmpty(domainKey)) return false;

            // Get the configured key for the domain
            var configuredDomainKey = Convert.ToString(ConfigurationManager.AppSettings["DrewEverestAPIKey"]);
            //GetConfiguredDomainKey(domainName);

            // Now compare the two
            return Equals(String.Compare(domainKey, configuredDomainKey, StringComparison.InvariantCulture), 0);
        }
    }
}